#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pop.hub


def start():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="pkgr")
    hub.pop.sub.add(dyne_name="pop_build")
    hub.pkgr.init.cli()
