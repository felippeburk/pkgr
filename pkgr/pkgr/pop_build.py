# Import python libs
import os
import shutil
import subprocess
import sys


def build(hub):
    """
    Build the package using pop-build
    """
    # Copy the required files into salt dir
    # build with pop_build with build.conf file
    sources = os.path.join(hub.pkgr.SDIR, "salt")
    for fn in os.listdir(hub.OPT.pkgr.sources):
        full = os.path.join(hub.OPT.pkgr.sources, fn)
        shutil.copy(full, sources)

    os.chdir(sources)
    subprocess.run("pop-build --config build.conf", shell=True)

    # create versioned tarfile
    os.chdir("dist")
    tcmd = f"tar -czvf salt-{hub.pkgr.VER}.tar.gz *"
    subprocess.run(tcmd, shell=True)
