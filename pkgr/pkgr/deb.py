# Import python libs
import subprocess
import os
import shutil
import sys
import datetime

# Import third party libs
import jinja2


def build(hub):
    """
    Build the package!
    """
    # copy sources into the build tree
    # name them by version
    # create source tarballs
    # place the changelog file down with the changes from the jinja template
    # debuild --no-sign -F
    # copy results of build to artifacts folder

    cp_cmd = f"cp -a -t {hub.pkgr.BDIR} {hub.pkgr.SDIR}/*"
    print(f"[pkgr] Running: {cp_cmd}")
    sys.stdout.flush()
    subprocess.run(cp_cmd, shell=True)
    proj_dir = ""
    relative_proj_dir = ""
    # change to the directory we extracted, bail if there is not exactly 1 directory
    with os.scandir(hub.pkgr.BDIR) as it:
        dirs = []
        for entry in it:
            if entry.is_dir():
                dirs.append(entry)
        if len(dirs) != 1:
            raise ValueError('Multiple directories found in build dir')
        else:
            proj_dir = os.path.join(hub.pkgr.BDIR, dirs[0].name)
            relative_proj_dir = dirs[0].name
    versioned_proj_dir = proj_dir + "_" + hub.pkgr.VER.replace('_','-') + '+ds'
    relative_versioned_proj_dir = relative_proj_dir + "_" + hub.pkgr.VER.replace('_','-') + '+ds'
    print(f"[pkgr] Renaming {proj_dir} to {versioned_proj_dir}")
    os.rename(proj_dir, versioned_proj_dir)
    rm_cmd = f"rm -rf {versioned_proj_dir}/.git"
    print(f"[pkgr] Running: {rm_cmd}")
    sys.stdout.flush()
    subprocess.run(rm_cmd, shell=True)
    tcmd = f"tar -acvf {versioned_proj_dir}.orig.tar.xz -C {hub.pkgr.BDIR} {relative_versioned_proj_dir}"
    print(f"[pkgr] Running: {tcmd}")
    sys.stdout.flush()
    subprocess.run(tcmd, shell=True)

    cp_cmd = f"cp -a -t {versioned_proj_dir} {hub.OPT.pkgr.debian_dir}"
    print(f"[pkgr] Running: {cp_cmd}")
    sys.stdout.flush()
    subprocess.run(cp_cmd, shell=True)
    changelog = os.path.join(versioned_proj_dir, 'debian/changelog')
    print(f"[pkgr] Writing changelog to {changelog}:")
    print(hub.pkgr.CHANGELOG)
    with open(changelog, "w+") as wfh:
        wfh.write(hub.pkgr.CHANGELOG)
    tcmd = f"tar -acvf {versioned_proj_dir}-1.debian.tar.xz -C {versioned_proj_dir} debian"
    print(f"[pkgr] Running: {tcmd}")
    sys.stdout.flush()
    subprocess.run(tcmd, shell=True)

    print(f"[pkgr] Changing Directory to {versioned_proj_dir}")
    os.chdir(versioned_proj_dir)
    cmd = f"debuild --no-sign -F"
    print(f"[pkgr] Running: {cmd}")
    sys.stdout.flush()
    retcode = subprocess.run(cmd, shell=True).returncode
    if retcode != 0:
        print(f"[pkgr] {cmd} exited with return code: {retcode}")
        sys.exit(retcode)

    print(f"[pkgr] Changing Directory to {hub.pkgr.CDIR}")
    os.chdir(hub.pkgr.CDIR)
    artifacts = os.path.join(hub.pkgr.CDIR, 'artifacts')
    if not os.path.isdir(artifacts):
        print(f"[pkgr] Making Directory: {artifacts}")
        os.mkdir(artifacts)
    with os.scandir(hub.pkgr.BDIR) as it:
        for entry in it:
            if entry.is_file():
                print(f"[pkgr] Moving {entry.name} to {artifacts}")
                shutil.copy(os.path.join(hub.pkgr.BDIR, entry.name), artifacts)
    sys.stdout.flush()

def render(hub):
    """
    Render the changelog file
    """
    opts = dict(hub.OPT.pkgr)
    opts["version"] = hub.pkgr.VER.replace('_','-')
    opts["changelog_date"] = datetime.datetime.now(tz=datetime.timezone.utc).strftime("%a, %d %b %Y %H:%M:%S %z")
    with open(os.path.join(hub.pkgr.CDIR, hub.OPT.pkgr.debian_dir, 'changelog')) as rfh:
        data = rfh.read()
    template = jinja2.Template(data)
    hub.pkgr.CHANGELOG = template.render(**opts)
