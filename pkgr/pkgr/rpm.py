# Import python libs
import subprocess
import os
import shutil

# Import third party libs
import jinja2


def build(hub):
    """
    Build the package!
    """
    # create proj.txz
    # Prep the build tree
    # Copy the sources in
    # place the spec file down
    # run rpmbuild -ba
    proj = os.path.join(hub.pkgr.SDIR, "proj.txz")
    os.chdir(hub.pkgr.SDIR)
    tcmd = f"tar --exclude-vcs -C {hub.pkgr.SDIR} -cvaf {proj} *"
    subprocess.run(tcmd, shell=True)
    os.chdir(hub.pkgr.CDIR)

    for dir_ in ("SOURCES", "SPEC", "BUILD", "SRPMS"):
        tdir = os.path.join(hub.pkgr.BDIR, dir_)
        os.makedirs(tdir)
    spec = os.path.join(hub.pkgr.BDIR, "SPEC", "pkg.spec")
    sources = os.path.join(hub.pkgr.BDIR, "SOURCES")
    rpms = os.path.join(hub.pkgr.BDIR, "RPMS")
    srpms = os.path.join(hub.pkgr.BDIR, "SRPMS")
    shutil.copy(proj, sources)
    for fn in os.listdir(hub.OPT.pkgr.sources):
        full = os.path.join(hub.OPT.pkgr.sources, fn)
        shutil.copy(full, sources)
    with open(spec, "w+") as wfh:
        wfh.write(hub.pkgr.SPEC)
    cmd = f"rpmbuild --define '_topdir {hub.pkgr.BDIR}' -ba {spec}"
    subprocess.run(cmd, shell=True)
    cwd = os.getcwd()
    for root, dirs, files in os.walk(rpms):
        for fn in files:
            if fn.endswith("rpm"):
                full = os.path.join(root, fn)
                shutil.copy(full, cwd)
    for root, dirs, files in os.walk(srpms):
        for fn in files:
            if fn.endswith("rpm"):
                full = os.path.join(root, fn)
                shutil.copy(full, cwd)

def render(hub):
    """
    Render the spec file
    """
    opts = dict(hub.OPT.pkgr)
    opts["version"] = hub.pkgr.VER
    with open(hub.OPT.pkgr.spec) as rfh:
        data = rfh.read()
    template = jinja2.Template(data)
    hub.pkgr.SPEC = template.render(**opts)
